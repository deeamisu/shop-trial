<?php
include '../functions.php';
include '../parts/head.php';
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container">
        <div class="row">
            <div class="col-4"></div>

            <!-- Register form-->
            <div class="col-4">
                <?php
                if (isset($_GET['register']))
                {
                    ?>
                    <div class="row p-3">Completati toate campurile, pentru o inregistrare corecta.</div>
                    <?php
                }
                ?>

            <form action="../process/registerProcess.php" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Adresa email:</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                    <small id="emailHelp" class="form-text text-muted">Adresa ta de email va ramane confidentiala.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Alege username:</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" name="username">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword2">Parola:</label>
                    <input type="password" class="form-control" id="exampleInputPassword2" name="password">
                </div>
                <button type="submit" class="btn btn-primary my-3">Inregistrare</button>
            </form>
            </div>

            <div class="col-4"></div>
        </div>
        <div class="row py-5">
            <div class="col-5"></div>
            <div class="col-2">
                <a href="../index.php" style="font-style: italic"><h3>Inapoi la magazin</h3></a>
            </div>
            <div class="col-5"></div>
        </div>
    </div>
</div>
</body>
</html>