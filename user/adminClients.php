<?php
include '../functions.php';
include '../parts/head.php';
securityCheck();
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container" style="background: white">
        <div class="row">
            <div class="col-12">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="adminProducts.php">Produse</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Clienti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="adminProductsSale.php">Vanzari</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../process/logOut.php">Log Out</a>
                    </li>
                </ul>
            </div>
        </div>

        <!--Afisez toti clientii intr-un tabel, cu optiunea de a-i elimina-->
        <?php
            $users=\Classes\User::findBy();
            $admin=array_shift($users);
        ?>
        <div class="row" id="ClientList">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Mail client:</th>
                        <th scope="col">Nume:</th>
                        <th scope="col">Produse:</th>
                        <th scope="col">Comanda:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach ($users as $item):
                            ?>
                            <tr>
                                <td><?php echo $item->getEmail();?></td>
                                <td><?php echo $item->getUsername();?></td>
                                <td>
                                    <?php
                                        foreach ($item->getCartProducts() as $cartProduct):
                                            ?>
                                            <p><?php echo $cartProduct->name?></p>
                                            <?php
                                        endforeach;
                                    ?>
                                </td>
                                <td><a class="btn btn-danger" href="../process/deleteClient.php?id=<?php echo $item->id;?>"
                                       role="button">Sterge</a></td>
                            </tr>
                            <?php
                        endforeach;
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
</body>
</html>
