<?php
include '../functions.php';
include '../parts/head.php';
securityCheck();
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container" style="background: white">
        <div class="row">
            <div class="col-12">

                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Produse</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="adminClients.php">Clienti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="adminProductsSale.php">Vanzari</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../process/logOut.php">Log Out</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row" id="ProductList">
            <!-- Afisez produsele intr-un tabel dupa nume si pret si creez link-uri
                pentru a putea intra pe pagina produsului in vederea editarii sau a stergerii-->
            <?php
            $products= \Classes\Product::findBy(null,null,'name');
            ?>

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Denumire produs</th>
                    <th scope="col">Categorie</th>
                    <th scope="col">Pret</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($products as $product):
                        ?>
                        <tr>
                            <td>
                                <a style="color: black" href="adminEditProduct.php?id=<?php echo $product->id;?>">
                                    <?php echo $product->name;?>
                                </a>
                            </td>
                            <td>
                                <a style="color: black" href="adminEditProduct.php?id=<?php echo $product->id;?>">
                                    <?php echo $product->getCategory()->name;?>
                                </a>
                            </td>
                            <td>
                                <a style="color: black" href="adminEditProduct.php?id=<?php echo $product->id;?>">
                                    <?php echo $product->price;?> RON
                                </a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>