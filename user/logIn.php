<?php
include '../functions.php';
include '../parts/head.php';
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Inregistrare</h5>
                        <p class="card-text">Pentru a putea finaliza comenzi ,inregistrarea ca nou client este obligatorie!</p>
                        <a href="register.php" class="btn btn-primary">INREGISTRARE</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Autentificare</h5>
                        <p class="card-text">Daca esti deja clientul nostru, autentifica-te pentru a putea plasa comenzi!</p>
                        <a href="authentification.php" class="btn btn-primary">AUTENTIFICARE</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row py-5">
            <div class="col-5"></div>
            <div class="col-2">
                <a href="../index.php" style="font-style: italic"><h3>Inapoi la magazin</h3></a>
            </div>
            <div class="col-5"></div>
        </div>
    </div>
</div>
</body>
</html>