<?php
include '../functions.php';
include '../parts/head.php';
securityCheck();

//$products=queryString("SELECT * FROM product WHERE id=".intval($_GET['id']));
$product = new \Classes\Product($_GET['id']);
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container" style="background: white">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
        <form class="p-4" method="post" action="../process/editProductProcess.php?id=<?php echo intval($_GET['id']);?>">
            <div class="form-group row">
                <label for="productName" class="col-sm-2 col-form-label">Nume:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="productName" name="name"
                           value="<?php echo $product->name;?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="category" class="col-sm-4 col-form-label">Categorie:</label>
                <div class="col-sm-8">
                    <select id="category" name="type">
                        <option value=1 <?php echo selected($product,1);?>>Rochii latino</option>
                        <option value=2 <?php echo selected($product,2);?>>Rochii standard</option>
                        <option value=3 <?php echo selected($product,3);?>>Incaltaminte femei</option>
                        <option value=4 <?php echo selected($product,4);?>>Top barbati</option>
                        <option value=5 <?php echo selected($product,5);?>>Pantaloni barbati</option>
                        <option value=6 <?php echo selected($product,6);?>>Incaltaminte barbati</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">Pret:</label>
                <div class="col-sm-8">
                    <input type="number" step="0.01" class="form-control" id="price" name="price" min="0"
                           value="<?php echo $product->price;?>">
                </div>
                <div class="col-sm-2">RON</div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Modifica</button>
                </div>
            </div>
        </form>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</div>
</body>
</html>