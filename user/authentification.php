<?php
include '../functions.php';
include '../parts/head.php';
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container">
        <div class="row">
            <div class="col-4"></div>

            <!-- Login form-->
            <div class="col-4">
                <?php
                if (isset($_GET['register']))
                {
                    ?>
                    <div class="row p-3">Date incorecte!</div>
                    <?php
                }
                ?>

                <form action="../process/authentificationProcess.php" method="post">
                    <div class="form-group">
                        <label for="exampleInputPassword3">Username:</label>
                        <input type="text" class="form-control" id="exampleInputPassword3" name="username">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword4">Parola:</label>
                        <input type="password" class="form-control" id="exampleInputPassword4" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary my-3">Autentificare</button>
                </form>
            </div>
            <div class="col-4"></div>
        </div>
        <div class="row py-5">
            <div class="col-5"></div>
            <div class="col-2">
                <a href="../index.php" style="font-style: italic"><h3>Inapoi la magazin</h3></a>
            </div>
            <div class="col-5"></div>
        </div>
    </div>
</div>
</body>
</html>