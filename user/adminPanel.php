<?php
include '../functions.php';
include '../parts/head.php';
securityCheck();
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container" style="background: white">
        <div class="row">
            <div class="col-12">

            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link" href="adminProducts.php">Produse</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="adminClients.php">Clienti</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="adminProductsSale.php">Vanzari</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../process/logOut.php">Log Out</a>
                </li>
            </ul>

            </div>
        </div>


    </div>
</div>
</body>
</html>