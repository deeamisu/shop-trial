<?php
include '../functions.php';
include '../parts/head.php';
securityCheck();
$sales=querystring('SELECT product.name AS Produs, SUM(sales.number) AS Cantitate, product.price AS Pret,
                              round(SUM(sales.number) * product.price, 2) AS Total 
                              FROM product 
                              left JOIN sales ON product.id=sales.product_id
                              GROUP BY product.name');
$salesKeys=array_keys($sales[0]);
$profit=queryString('SELECT round(sum(product.price), 2) AS total FROM product
                                INNER JOIN sales ON sales.product_id=product.id');
?>
<body>
<div class="container-fluid p-4" style="background-color: #F6EADB;">
    <div class="container" style="background: white">
        <div class="row">
            <div class="col-12">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="adminProducts.php">Produse</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="adminClients.php">Clienti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Vanzari</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../process/logOut.php">Log Out</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Tabel produse si cantitati vandute -->
        <div class="row">
            <div class="col">

                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <?php
                            foreach ($salesKeys as $key=>$value):
                                ?>
                                <th scope="col"><?php echo $value;?></th>
                                <?php
                            endforeach;
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($sales as $sale):
                        ?>
                        <tr>
                            <?php
                            foreach ($sale as $value):
                                ?>
                                <td><?php echo $value;?></td>
                                <?php
                            endforeach;
                            ?>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <th class="bg-success">Total Vanzari</th>
                        <th class="bg-success"><?php echo $profit[0]['total'];?> RON</th>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="row m-3 text-center">
            <div class="col">
                <a href="adminProductsSalePDF.php">VEZI PDF</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>