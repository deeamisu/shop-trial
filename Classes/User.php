<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 6/21/2020
 * Time: 1:59 AM
 */
class User extends Base
{
    public $email;

    public $username;

    public $password;

    public $cart;

    /**
     * User constructor.
     * @param $id
     * @param $email
     * @param $username
     * @param $cart
     */

    /**
     * @return Product[]
     */
    public function getCartProducts()
    {
        $data=queryString("SELECT product_id FROM cart WHERE user_id=".$this->getId());
        $list=[];

        foreach ($data as $datum):
            $list[]=new Product($datum['product_id']);
        endforeach;
        return $list;
    }

    /**
     * @return int|mixed
     */
    public function getCartTotalPrice()
    {
        $products=$this->getCartProducts();
        $sum=0;
        foreach ($products as $product):
            $sum+=$product->getPrice();
        endforeach;
        return $sum;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    static public function getClassName()
    {
        return 'user';
    }

}