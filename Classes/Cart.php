<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 6/25/2020
 * Time: 2:07 AM
 */
class Cart extends Base
{
    public $product_id;

    public $user_id;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     * @return Cart
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Cart
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    static public function getClassName()
    {
       return 'cart';
    }
}