<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 6/24/2020
 * Time: 2:48 AM
 */
abstract class Base
{
    public  $id;

    /**
     * Base constructor.
     * @param $id
     */
    public function __construct($id=null)
    {
            $this->setId($id);
        if (!is_null($id))
        {
            $data=queryString("SELECT * FROM ".$this->getClassName()." WHERE id=".intval($this->getId()));
            foreach ($data[0] as $key=>$value)
            {
                $this->$key=$value;
            }
        }

    }

    public function create()
    {
        $values=get_object_vars($this);
        unset($values['id']);
        if (array_key_exists('cart',$values)):
            unset($values['cart']);
        endif;
        $keys=array_keys($values);
        $dbkeys=implode(',',$keys);
        $dbvals=[];
        foreach ($values as $value):
            $dbvals[]=$value;
        endforeach;
        $dbvalues=implode("','",$dbvals);

        $this->setId(queryString("INSERT INTO ".$this->getClassName()."(".$dbkeys.") VALUES ('".$dbvalues."')"));
    }

    public function delete()
    {
        queryString("DELETE FROM ".$this->getClassName()." WHERE id=".$this->getId());
    }

    public function update()
    {
        $values=get_object_vars($this);
        unset($values['id']);
        if (array_key_exists('image',$values)):
            unset($values['image']);
        endif;
        if (array_key_exists('discount',$values)):
            unset($values['discount']);
        endif;
        $updateValues=[];
        foreach ($values as $key=>$value):
            $updateValues[]=$key."='".$value."'";
        endforeach;
        $updateString=implode(",",$updateValues);
        queryString("UPDATE ".$this->getClassName()." SET ".$updateString." WHERE id=".$this->getId());
    }

    static public function findBy($filters=null,$search=null,$orderBy=null,$order='ASC',$limit=null,$offset=0)
    {
        $querry='SELECT * FROM '.strtolower(static::getClassName());
        if (!is_null($filters)):
            $parts=[];
            foreach ($filters as $key=>$filter):
                if (!is_null($search))
                {
                    $parts[]=$key." LIKE '%".$filter."%'";
                }
                else
                {
                    $parts[]=$key.' = '.$filter;
                }
            endforeach;
            $partsWhere=' WHERE '.implode(' AND ',$parts);
            $querry.=$partsWhere;
        endif;
        if (!is_null($orderBy)):
            $querry.=" ORDER BY ".$orderBy." ".$order;
        endif;
        if (!is_null($limit)):
            $querry.=" LIMIT ".$offset.",".$limit;
        endif;
        $data=queryString($querry);
        $list=[];
        $class=static::class;
        foreach ($data as $datum):
              $list[]=$class::fromArray($datum);
        endforeach;
        return $list;
    }

    static public function filterByValue($data,$field,$min=0,$max=999999999999)
    {
        $productList=[];
        foreach ($data as $key=>$datum)
        {
            if (($datum->$field>=$min) && ($datum->$field<=$max))
            {
                $productList[]=$datum;
            }
        }
        return $productList;
    }

    static public function fromArray($line)
    {
        $className=static::class;
        $object=new $className;
        foreach ($line as $key=>$value):
            $object->$key=$value;
        endforeach;
        return $object;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Base
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    abstract static public function getClassName();
}