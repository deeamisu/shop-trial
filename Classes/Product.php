<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 6/20/2020
 * Time: 2:10 PM
 */
class Product extends Base
{
    public $name;

    public $image;

    public $price;

    public $discount;

    public $category_id;

    /**
     * Product constructor.
     * @param $id
     * @param $name
     * @param $image
     * @param $price
     * @param $discount
     * @param $category_id
     */


    /**
     * @return Category
     */
    public function getCategory()
    {
        $category = new Category($this->category_id);
        return $category;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getCartId($userId)
    {
        $data = Cart::findBy(['product_id'=>$this->getId(),'user_id'=>$userId]);
        return $data[0]->getId();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return Product
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     * @return Product
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    /**
     * @return string
     */
    static public function getClassName()
    {
        return 'product';
    }

}