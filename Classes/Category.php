<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 6/20/2020
 * Time: 2:20 PM
 */
class Category extends Base
{
    public $name;


    public function getProducts($min=0,$max=999999999.99)
    {
        return Product::filterByValue(Product::findBy(['category_id'=>intval($this->getId())]),'price',$min,$max);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    static public function getClassName()
    {
       return 'category';
    }
}