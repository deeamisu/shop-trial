<?php
    include 'functions.php';
    include 'parts/head.php';
?>
<body>
    <div id="mainBackgound" class="container-fluid mainBackground p-4">
        <div id="secondBackground" class="container-fluid secondBackground">
            <?php
                include 'parts/header.php';
                include 'parts/content.php';
                include 'parts/footer.php';
            ?>
        </div>
    </div>
</body>
</html>