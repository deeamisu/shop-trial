<div id="content" class="row">
    <div class="col-sm-3 col-md-1"></div>
    <div id="leftMenu" class="col-sm-3 col-md-3"><?php include 'leftMenu.php';?></div>
    <div id="productDemo" class="col-sm-6"><?php showProducts(populate());?></div>
    <div class="col-sm-1"></div>
</div>
<div id="pagination" class="row">
    <div class="col-12">
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item <?php echo pageCheck($_SESSION['page'],1)?>">
                <a class="page-link" href="index.php?page=-1">Previous</a>
            </li>
            <li class="page-item <?php echo pageCheck($_SESSION['page'],$lastPage)?>">
                <a class="page-link" href="index.php?page=1">Next</a>
            </li>
        </ul>
    </nav>
    </div>
</div>