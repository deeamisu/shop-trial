
<div class="container" style="margin-top: 30px">
    <div class="row fontBlack">
        <div id="menu" class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Meniu</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?reset=true">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Categorii
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="index.php?cat=1">Rochii latino</a>
                                <a class="dropdown-item" href="index.php?cat=2">Rochii standard</a>
                                <a class="dropdown-item" href="index.php?cat=3">Incaltaminte femei</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="index.php?cat=4">Top barbati</a>
                                <a class="dropdown-item" href="index.php?cat=5">Pantaloni barbati</a>
                                <a class="dropdown-item" href="index.php?cat=6">Incaltaminte barbati</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Despre noi</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0" action="process/search.php" method="post">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        </div>
    </div>
</div>