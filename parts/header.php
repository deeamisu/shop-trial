
<div id="header" class="row">
    <div class="col-md-2 col-lg-4 d-dm:none d:md-block"></div>
    <div class="col-sm-12 col-lg-10 col-lg-4 titleFontFamily text-wrap">
        <h1 class="text-center">DanceWeare Style</h1>
    </div>
    <div class="col-md-2 col-lg-4 d-dm:none d:md-block"></div>
</div>
<div class="row fontBlack py-3">
    <div class="col-sm-6 col-md-9"></div>
    <div id="userLog" class="col-sm-2 col-md-1">
        <?php
            if (isset($_SESSION['user']))
            {
                ?>
                <i class="fas fa-user"><?php echo $_SESSION['user']->username;?></i>
                <?php
            }
            else
            {
                ?>
                <a href="user/logIn.php"><i class="fas fa-user"> Log In</i></a>
                <?php
            }
        ?>
    </div>
    <div id="shoppingCart" class="col-sm-2 col-md-1">
        <a href="shoppingCart.php"><i class="fas fa-shopping-cart"> Cos</i></a>
    </div>
    <div id="logOut" class="col-sm-2 col-md-1">
        <a href="process/logOut.php"><i class="fas fa-user-slash"> Log Out</i></a>
    </div>
</div>
<div id="menu" class="row">
    <?php include 'menu.php';?>
</div>