<?php
include 'functions.php';
include 'parts/head.php';
?>
<body>
    <div id="mainBackgound" class="container-fluid mainBackground p-4">
        <div id="secondBackground" class="container-fluid secondBackground">
<?php
    include 'parts/header.php';

    if (!(isset($_SESSION['user'])))
    {
        ?>
        <div class="alert alert-warning text-center" role="alert">
            Utilizator neidentificat. Cos de cumparaturi indisponibil!
        </div>
        <?php
    }
    else
    {
        $products=$_SESSION['user']->getCartProducts();
        ?>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Denumire produs</th>
                    <th scope="col">Pret</th>
                    <th scope="col">Comanda</th>
                </tr>
                </thead>
                <tbody>
        <?php
            foreach ($products as $key=>$product)
            {
                ?>
                <tr>
                    <td>
                        <?php echo $product->getName();?>
                    </td>
                    <td>
                        <?php echo $product->getPrice();?>
                    </td>
                    <td>
                        <a class="btn btn-danger" href="process/deleteCartProduct.php?id=<?php echo $product->getCartId($_SESSION['user']->getId());?>" role="button">
                            Elimina</a>
                    </td>
                </tr>
                <?php
            }
        ?>
                </tbody>
            </table>
        <div class="row">
            <div class="col-sm-6">
                <h2 class="text-primary text-center">Total factura:</h2>
            </div>
            <div class="col-sm-6">
                <h2 class="text-primary text-center"><?php echo $_SESSION['user']->getCartTotalPrice();?> RON</h2>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <a class="btn btn-info" href="user/shopingBill.php" role="button">Vezi Factura</a>
            </div>
        </div>
        <?php
    }
    include 'parts/footer.php';
?>
        </div>
    </div>
</body>
</html>

