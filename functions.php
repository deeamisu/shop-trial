<?php

spl_autoload_register('autoload');

session_start();
$mysql=mysqli_connect('188.240.210.8','root','scoalait123','mihai_db');

use Classes\Product;
use Classes\Cart;
use Classes\Category;
use Classes\User;

if (!isset($_GET['page']))
{
    $_SESSION['page']=1;
}
else
{
    $_SESSION['page']+=intval($_GET['page']);
}

//Setez id-ul produzului selectat, in cazul in care a fost selectat -- afisare in pagina produsului
if (isset($_GET['idProduct']))
{
    $getProduct= new Product(intval($_GET['idProduct']));
}

//Setez categoria , in cazul in care a fost selectata
if (isset($_GET['cat']))
{
    $_SESSION['cat']=$_GET['cat'];
    unset($_SESSION['reset']);
    unset($_SESSION['search']);
    $_SESSION['page']=1;
}

//Setez filtrul pentru preturi, in cazul in care a fost selectat
if (isset($_GET['min']))
{
    if ($_GET['min']=='reset')
    {
        unset($_SESSION['min']);
        unset($_SESSION['max']);
    }
    else
    {
        $_SESSION['min']=intval($_GET['min']);
        $_SESSION['max']=intval($_GET['max']);
        $_SESSION['page']=1;
    }
}

//Resetez categoria, in cazul in care se revine la pagina initiala
if (isset($_GET['reset']))
{
    unset($_SESSION['cat']);
    unset($_SESSION['min']);
    unset($_SESSION['max']);
    unset($_SESSION['search']);
    $_SESSION['page']=1;
    $_SESSION['reset']=$_GET['reset'];
}

//Filtrez dupa cautare
//Slecetez doar produsele din categoria selectata, sau , daca nu este selectata nici o categorie selectata,
//  selectez toate produsele. Aplic de asemenea si filtrul de preturi, daca exista.
if (isset($_SESSION['search']))
{
    if (isset($_SESSION['min']))
    {
        $selectedProducts=Product::filterByValue(Product::findBy(['name'=>mysqli_real_escape_string($mysql,$_SESSION['search'])],
            'true','name','ASC'),'price',$_SESSION['min'],$_SESSION['max']);
        $products=filterProductsPrice($selectedProducts);

        $lastPage=ceil(count(
            Product::filterByValue(Product::findBy(['name'=>mysqli_real_escape_string($mysql,$_SESSION['search'])],
                'true','name','ASC'),'price',$_SESSION['min'],$_SESSION['max'])
            )/6);
    }
    else
    {
        $products=Product::findBy(['name'=>mysqli_real_escape_string($mysql,$_SESSION['search'])],'true','name',
                                    'ASC',6,($_SESSION['page']-1)*6);
        $lastPage=ceil(count(Product::findBy(['name'=>mysqli_real_escape_string($mysql,$_SESSION['search'])],'true',
                        'name','ASC'))/6);
    }
}
else
{
    if (isset($_SESSION['cat']))
    {
        $category=new Category(mysqli_real_escape_string($mysql,intval($_SESSION['cat'])));
        if (isset($_SESSION['min']))
        {
            $selectedProducts=$category->getProducts($_SESSION['min'],$_SESSION['max']);
        }
        else
        {
            $selectedProducts=$category->getProducts();
        }
        $products=filterProductsPrice($selectedProducts);
        $lastPage=ceil(count($selectedProducts)/6);
    }
    else
    {
        if (isset($_SESSION['min']))
        {

            $selectedProducts=Product::filterByValue(Product::findBy(),'price',$_SESSION['min'],$_SESSION['max']);
            $products=filterProductsPrice($selectedProducts);
            $lastPage=ceil(count(Product::filterByValue(Product::findBy(),'price',$_SESSION['min'],$_SESSION['max']))/6);
        }
        else
        {
            $products=Product::findBy(null,null,null,'ASC',6,($_SESSION['page']-1)*6);
            $lastPage=ceil(count(Product::findBy())/6);
        }
    }
}


function queryString($sql_command)
{
    global $mysql;
    $result=mysqli_query($mysql,$sql_command);
    if ($result===false)
    {
        die($sql_command);
    }
    if (!($result===true))
    {
        return(mysqli_fetch_all($result,MYSQLI_ASSOC));
    }
    if ($result===true)
    {
        return mysqli_insert_id($mysql);
    }
}

function populate()
{
    global $products;
    $keyProducts=array_keys($products);
    shuffle($keyProducts);
    $randomProducts=[];
    foreach ($keyProducts as $key):
        $randomProducts[$key]=$products[$key];
    endforeach;
    return $randomProducts;
}

function showProducts($whatToShow)
{
    $index=0;
    ?>
        <div class="container-fluid" style="margin-top: 30px">
                 <?php
                    foreach ($whatToShow as $item):
                    if ($index==0)
                    {
                        ?>
                        <div class="row my-4">
                        <?php
                    }
                    ?>
                            <div class="col-sm-1 col-md-1 col-lg-1"></div>
                            <div class="col-sm-6 col-md-4 col-lg-3 fontBlack">
                                <a href="product.php?idProduct=<?php echo $item->id;?>">

                                    <div class="card" style="width: 18rem;">
                                        <img src="images/<?php echo $item->image;?>" class="card-img-top" width="175px" height="175px">
                                        <div class="card-body">
                                            <p class="card-text text-center"><?php echo $item->price;?> RON</p>
                                        </div>
                                    </div>

                                    <!--<img src="images/<?php //echo $item->image;?>" width="200px" height="200px">-->
                                </a>
                            </div>
                    <?php
                    $index++;
                    if ($index==3)
                    {
                        ?>
                        </div>
                        <?php
                        $index=0;
                    }
                    endforeach;
                    if ($index>0):
                        ?></div><?php
                    endif;
                 ?>
        </div>
    <?php
}

function isSelected($val)
{
    if (isset($_SESSION[$val]))
    {
        return " active ";
    }
}

function showProductDetails($product)
{
    ?>
    <div class="cntainer">
        <div class="row">
            <div class="col-12 p-4 text-center titleFontFamily">
                <h2><?php echo $product->name?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-7 p-4 text-center">
                <img src="images/<?php echo $product->image?>">
            </div>

            <div class="col-5 p-4 text-center">
                <h3><?php echo $product->price?> RON</h3>

                <!-- Urmeaza sa introduc buton adauga in cos-->

                <!-- Button trigger modal -->

                <a class="btn btn-success" role="button" <?php echo startModal($product);?>>
                    <?php echo succesfullAddToCart();?></a>


                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Avertisment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h4>Pentru a putea adauga produse in cosul de cumparaturi este necesara inregistrarea si autentificarea utilizatorului.</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Finish cos-->

            </div>
        </div>
    </div>
    <?php
}

function selected($product,$text)
{
    if ($product->category_id==$text):
        return "selected";
    endif;
}

function securityCheck()
{
    if ($_SESSION['user']->id<>1)
    {
        header('Location:../index.php');
        die();
    }
}

function startModal($product)
{
    if (isset($_SESSION['user']))
    {
        return 'href="process/addToCart.php?id='.$product->id.'" ';
    }
    else
    {
        return "href='#' data-toggle=\"modal\" data-target=\"#exampleModal\"";
    }
}

function succesfullAddToCart()
{
    if (isset($_SESSION['succesfullAdd']))
    {
        unset($_SESSION['succesfullAdd']);
        return 'Produs adaugat in cos';
    }

    return 'Adauga in cos';
}

function productSearch($text="",$min=0,$max=999999999.99)
{
    $list=[];
    $data=queryString("SELECT * FROM product WHERE name LIKE '%".$text."%' 
                                        AND price>".$min." AND price<".$max);
    foreach ($data as $item):
        $list[]=new Product($item['id']);
    endforeach;
    return $list;
}

function pageCheck($page,$pageToCheck)
{
    if ($page==$pageToCheck)
    {
        return ' disabled';
    }
}

function filterProductsPrice($selectedProducts)
{
    $index=($_SESSION['page']-1)*6;
    $products=[];
    for ($i=0;$i<6;$i++):
        if (key_exists($index,$selectedProducts))
        {
            $products[]=$selectedProducts[$index];
            $index++;
        }
        else
        {
            break;
        }
    endfor;
    return $products;
}

function autoload($class)
{
    $path=str_replace("\\",DIRECTORY_SEPARATOR,$class);
    include $path.'.php';
}