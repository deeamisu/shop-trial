<?php
include '../functions.php';
securityCheck();

$product=\Classes\Product::fromArray(['id'=>intval($_GET['id']),
                            'name'=>mysqli_real_escape_string($mysql,$_POST['name']),
                            'price'=>mysqli_real_escape_string($mysql,$_POST['price']),
                            'category_id'=>mysqli_real_escape_string($mysql,$_POST['type']),]);
$product->update();

header('Location:../user/adminProducts.php');
die();
