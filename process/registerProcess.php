<?php
include '../functions.php';

if ($_POST['email']=="" || $_POST['username']=="" || $_POST['password']=="")
{
    header('Location:../user/register.php?register=false');
    die();
}
else
{
    $user=\Classes\User::fromArray(['email'=>mysqli_real_escape_string($mysql,$_POST['email']),
                            'username'=>mysqli_real_escape_string($mysql,$_POST['username']),
                            'password'=>mysqli_real_escape_string($mysql,md5($_POST['password']))]);
    $user->create();
    header('Location:../user/logIn.php');
}